/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyect.Modelo;

import java.util.ArrayList;
import java.util.Scanner;

public class cita {

    ArrayList fecha;
    ArrayList iddoctor;
    ArrayList idpaciente;
    ArrayList codservi;
    ArrayList codcita;

    public cita(ArrayList fecha, ArrayList iddoctor, ArrayList idpaciente, ArrayList codservi, ArrayList codcita) {
        this.fecha = fecha;
        this.iddoctor = iddoctor;
        this.idpaciente = idpaciente;
        this.codservi = codservi;
        this.codcita = codcita;
    }

    public ArrayList getFecha() {
        return fecha;
    }

    public void setFecha(ArrayList fecha) {
        this.fecha = fecha;
    }

    public ArrayList getIddoctor() {
        return iddoctor;
    }

    public void setIddoctor(ArrayList iddoctor) {
        this.iddoctor = iddoctor;
    }

    public ArrayList getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(ArrayList idpaciente) {
        this.idpaciente = idpaciente;
    }

    public ArrayList getCodservi() {
        return codservi;
    }

    public void setCodservi(ArrayList codservi) {
        this.codservi = codservi;
    }

    public ArrayList getCodcita() {
        return codcita;
    }

    public void setCodcita(ArrayList codcita) {
        this.codcita = codcita;
    }

    //controlador de citas
    public void Ingresarfecha(String fechas) {
        fecha.add(fechas);
    }

    public void Ingresarcodigoc(String codigoc) {
        codcita.add(codigoc);
    }

    public void Ingresarcodser(String codigoser) {
        codservi.add(codigoser);
    }

    public void Ingresaridpa(String idpa) {
        idpaciente.add(idpa);
    }

    public void Ingresardoctor(String iddoc) {
        iddoctor.add(iddoc);
    }

    public String Buscarcodigoc(String codigoc) {
        String buscarcc = "";
        if (codcita.contains(codigoc) == true) {
            buscarcc = codcita.get(codcita.indexOf(codigoc)).toString();
        } else {
            buscarcc = "no existe ";
        }
        return buscarcc;

    }

    public String Buscaridpa(String idpa) {
        String buscaridp = "";
        if (idpaciente.contains(idpa) == true) {
            buscaridp = idpaciente.get(idpaciente.indexOf(idpa)).toString();
        } else {
            buscaridp = "no existe ";
        }
        return buscaridp;

    }

    public String Buscariddoc(String iddoc) {
        String buscariddoc = "";
        if (iddoctor.contains(iddoc) == true) {
            buscariddoc = iddoctor.get(iddoctor.indexOf(iddoc)).toString();
        } else {
            buscariddoc = "no existe ";
        }
        return buscariddoc;

    }

    public String Buscarfecha(String fechas) {
        String buscarfecha = "";
        if (fecha.contains(fechas) == true) {
            buscarfecha = fecha.get(fecha.indexOf(fechas)).toString();
        } else {
            buscarfecha = "no existe ";
        }
        return buscarfecha;

    }

    public String Buscarcodser(String codser) {
        String buscarcodser = "";
        if (codservi.contains(codser) == true) {
            buscarcodser = codservi.get(codservi.indexOf(codser)).toString();
        } else {
            buscarcodser = "no existe ";
        }
        return buscarcodser;

    }

    public void Eliminarcodser(String codser) {
        codservi.remove(codservi.indexOf(codser));

    }

    public void Eliminarfecha(String fechas) {
        fecha.remove(fecha.indexOf(fechas));

    }

    public void Eliminaridpa(String idpa) {
        idpaciente.remove(idpaciente.indexOf(idpa));

    }

    public void Eliminariddoc(String iddoc) {
        iddoctor.remove(iddoctor.indexOf(iddoc));

    }

    public void Eliminarcodci(String codicita) {
        codcita.remove(codcita.indexOf(codicita));

    }

}
