/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyect.Modelo;

import java.util.ArrayList;





public abstract  class persona {
    ArrayList nombre,apellido, sexo, tpo, id, telefono,direccion,eps,sueldo,especialidad,horario;
    ArrayList edad;

    public persona(ArrayList nombre, ArrayList apellido, ArrayList sexo, ArrayList tpo, ArrayList id, ArrayList telefono, ArrayList direccion, ArrayList eps, ArrayList sueldo, ArrayList especialidad, ArrayList horario, ArrayList edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.sexo = sexo;
        this.tpo = tpo;
        this.id = id;
        this.telefono = telefono;
        this.direccion = direccion;
        this.eps = eps;
        this.sueldo = sueldo;
        this.especialidad = especialidad;
        this.horario = horario;
        this.edad = edad;
    }

    
    public ArrayList getNombre() {
        return nombre;
    }

    public void setNombre(ArrayList nombre) {
        this.nombre = nombre;
    }

    public ArrayList getApellido() {
        return apellido;
    }

    public void setApellido(ArrayList apellido) {
        this.apellido = apellido;
    }

    public ArrayList getSexo() {
        return sexo;
    }

    public void setSexo(ArrayList sexo) {
        this.sexo = sexo;
    }

    public ArrayList getTpo() {
        return tpo;
    }

    public void setTpo(ArrayList tpo) {
        this.tpo = tpo;
    }

    public ArrayList getId() {
        return id;
    }

    public void setId(ArrayList id) {
        this.id = id;
    }

    public ArrayList getTelefono() {
        return telefono;
    }

    public void setTelefono(ArrayList telefono) {
        this.telefono = telefono;
        
    }

    public ArrayList getDireccion() {
        return direccion;
    }

    public void setDireccion(ArrayList direccion) {
        this.direccion = direccion;
    }

    public ArrayList getEps() {
        return eps;
    }

    public void setEps(ArrayList eps) {
        this.eps = eps;
    }

    public ArrayList getSueldo() {
        return sueldo;
    }

    public void setSueldo(ArrayList sueldo) {
        this.sueldo = sueldo;
    }

    public ArrayList getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(ArrayList especialidad) {
        this.especialidad = especialidad;
    }

    public ArrayList getHorario() {
        return horario;
    }

    public void setHorario(ArrayList horario) {
        this.horario = horario;
    }
    

    public ArrayList getEdad() {
        return edad;
    }

    public void setEdad(ArrayList edad) {
        this.edad = edad;
    }
    
    
    public abstract void IngresarNombre(String nombres);
    public abstract void Ingresarapellido(String apellidos);
    public abstract void Ingresarsexo(String sexos);
    public abstract void Ingresartipo(String tipos);
    public abstract void Ingresarid(String ids);
    public abstract void Ingresartelefono(String telefonos);
    public abstract void Ingresardireccion(String direcciones);
    public abstract void Ingresareps(String epss);
    public abstract void Ingresarsueldo(String sueldos);
    public abstract void Ingresarespecialidad(String especialidades);
    public abstract void Ingresarhorario(String horarios);
    public abstract void Ingresaredad(int edads);
    
    public abstract void EliminarNombre(String nombres);
    public abstract void Eliminarapellido(String apellidos);
    public abstract void Eliminarsexos(String sexos);
    public abstract void Eliminartipos(String tipos);
    public abstract void Eliminarid(String ids);
    public abstract void Eliminartelefono(String telefono);
    public abstract void Eliminardireccion(String direcciones);
    public abstract void Eliminareps(String epss);
    public abstract void Eliminarsueldos(String sueldos);
    public abstract void Eliminarespecialidades(String especialidades);
    public abstract void Eliminaredad(int edads);
    
    public abstract String BuscarNombre(String nombres);
    public abstract String Buscarapellido(String apellidos);
    public abstract String Buscarsexos(String sexos);
    public abstract String Buscartipos(String tipos);
    public abstract String Buscarid(String ids);
    public abstract String Buscartelefono(String telefonos);
    public abstract String Buscardireccion(String direcciones);
    public abstract String Buscareps(String epss);
    public abstract String Buscarsueldo(String sueldos);
    public abstract String Buscarespecialidades(String especialidades);
    public abstract int Buscaredad(int edads);
    

   
   
        
    
}

