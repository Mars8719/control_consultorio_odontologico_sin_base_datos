/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyect.Modelo;

import proyect.Modelo.persona;
import java.util.ArrayList;




public class paciente extends persona {

    public paciente(ArrayList nombre, ArrayList apellido, ArrayList sexo, ArrayList tpo, ArrayList id, ArrayList telefono, ArrayList direccion, ArrayList eps, ArrayList sueldo, ArrayList especialidad, ArrayList horario, ArrayList edad) {
        super(nombre, apellido, sexo, tpo, id, telefono, direccion, eps, sueldo, especialidad, horario, edad);
    }
   
    @Override
    public void IngresarNombre(String nombres) {
        super.nombre.add(nombres); 
    }

    @Override
    public void Ingresarapellido(String apellidos) {
        super.apellido.add(apellidos); 
    }

    @Override
    public void Ingresarsexo(String sexos) {
        super.sexo.add(sexos); 
       
    }

    @Override
    public void Ingresartipo(String tipos) {
        super.tpo.add(tipos); 
        
    }

    @Override
    public void Ingresarid(String ids) {
        super.id.add(ids); 
       
    }

    @Override
    public void Ingresartelefono(String telefonos) {
        super.telefono.add(telefonos); 
    }

    @Override
    public void Ingresaredad(int edads) {
        super.edad.add(edads); 
        
    }
   
    
    @Override
    public void EliminarNombre(String nombres) {
        super.nombre.remove(super.nombre.indexOf(nombres)); 
        
      
    }

    @Override
    public void Eliminarapellido(String apellidos) {
        super.apellido.remove(super.apellido.indexOf(apellidos)); 
    }

    @Override
    public void Eliminarsexos(String sexos) {
        super.sexo.remove(super.sexo.indexOf(sexos)); 
        
    }

    @Override
    public void Eliminartipos(String tipos) {
        super.tpo.remove(super.tpo.indexOf(tipos)); 
       
    }

    @Override
    public void Eliminarid(String ids) {
       super.id.remove(super.id.indexOf(ids)); 
    }

    @Override
    public void Eliminartelefono(String telefonos) {
       super.telefono.remove(super.telefono.indexOf(telefonos)); 
    }

    @Override
    public void Eliminaredad(int edads) {
        super.edad.remove(super.edad.indexOf(edads)); 
        
    }
//controlador de paciente
    
    @Override
    public String BuscarNombre(String nombres) {
      String buscarnombre="";
       if(super.nombre.contains(nombres)==true){
           buscarnombre=super.nombre.get(super.nombre.indexOf(nombres)).toString();
       } else {
           buscarnombre="no existe ";
       }
       return buscarnombre;
    }

    @Override
    public String Buscarapellido(String apellidos) {
         String buscarapellido="";
       if(super.apellido.contains(apellidos)==true){
           buscarapellido=super.apellido.get(super.apellido.indexOf(apellidos)).toString();
       } else {
           buscarapellido="no existe ";
       }
       return buscarapellido;
      
    }

    @Override
    public String Buscarsexos(String sexos) {
         String buscarsexo="";
       if(super.sexo.contains(sexos)==true){
           buscarsexo=super.sexo.get(super.sexo.indexOf(sexos)).toString();
       } else {
           buscarsexo="no existe ";
       }
       return buscarsexo;
        
    }

    @Override
    public String Buscartipos(String tipos) {
         String buscartipos="";
       if(super.tpo.contains(tipos)==true){
           buscartipos=super.tpo.get(super.tpo.indexOf(tipos)).toString();
       } else {
           buscartipos="no existe ";
       }
       return buscartipos;
        
    }

    @Override
    public String Buscarid(String ids) {
         String buscarid="";
         //String buscarno="";
         
       if(super.id.contains(ids)==true){
           buscarid=super.id.get(super.id.indexOf(ids)).toString();
           //buscarno=super.nombre.get(super.nombre.indexOf(nombres)).toString();
           
       } else {
           buscarid="no existe ";
       }
       return buscarid;
       
       
        
    }

    @Override
    public String Buscartelefono(String telefonos) {
         String buscartelefono="";
       if(super.telefono.contains(telefonos)==true){
           buscartelefono=super.telefono.get(super.telefono.indexOf(telefonos)).toString();
       } else {
           buscartelefono="no existe ";
       }
       return buscartelefono;
       
    }

    @Override
    public int Buscaredad(int edads) {
         int buscaredad=0;
       if(super.edad.contains(edads)==true){
           buscaredad=Integer.parseInt(super.edad.get(super.edad.indexOf(edads)).toString());
       } else {
           buscaredad=0;
       }
       return buscaredad;
        
    }

    @Override
    public void Ingresardireccion(String direcciones) {
        super.direccion.add(direcciones); 
    }

    @Override
    public void Eliminardireccion(String direcciones) {
        super.direccion.remove(super.direccion.indexOf(direcciones)); 
        
    }

    @Override
    public String Buscardireccion(String direcciones) {
         String buscardireccion="";
       if(super.direccion.contains(direcciones)==true){
           buscardireccion=super.direccion.get(super.direccion.indexOf(direcciones)).toString();
       } else {
           buscardireccion="";
       }
       return buscardireccion;
        
    }

    @Override
    public void Ingresareps(String epss) {
         super.eps.add(epss); 
        
    }

    @Override
    public void Ingresarsueldo(String sueldos) {
         super.sueldo.add(sueldos); 
    }

    @Override
    public void Ingresarespecialidad(String especialidades) {
        super.especialidad.add(especialidades); 
    }

    @Override
    public void Ingresarhorario(String horarios) {
       super.horario.add(horarios); 
    }

    @Override
    public void Eliminareps(String epss) {
        super.eps.remove(super.eps.indexOf(epss)); 
      
    }

    @Override
    public void Eliminarsueldos(String sueldos) {
        super.sueldo.remove(super.sueldo.indexOf(sueldos)); 
    }

    @Override
    public void Eliminarespecialidades(String especialidades) {
        super.especialidad.remove(super.especialidad.indexOf(especialidades)); 
    }

    @Override
    public String Buscareps(String epss) {
          String buscareps="";
       if(super.eps.contains(epss)==true){
           buscareps=super.eps.get(super.eps.indexOf(epss)).toString();
       } else {
           buscareps="no existe ";
       }
       return buscareps;
       
    }

    @Override
    public String Buscarsueldo(String sueldos) {
         String buscarsueldo="";
       if(super.sueldo.contains(sueldos)==true){
           buscarsueldo=super.sueldo.get(super.sueldo.indexOf(sueldos)).toString();
       } else {
           buscarsueldo="no existe ";
       }
       return buscarsueldo;
    }

    @Override
    public String Buscarespecialidades(String especialidades) {
          String buscarespecialidad="";
       if(super.especialidad.contains(especialidades)==true){
           buscarespecialidad=super.especialidad.get(super.especialidad.indexOf(especialidades)).toString();
       } else {
           buscarespecialidad="no existe ";
       }
       return buscarespecialidad;
    }
   

    
    
   
        
   
    
   
    
    
}
